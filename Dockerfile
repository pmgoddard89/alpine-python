FROM python:3.7.1-alpine3.8
RUN apk add --no-cache --virtual .build-deps \
    gcc \
    gfortran \
    build-base \
    && apk add --no-cache openblas-dev \
    && pip3 install --upgrade --no-cache-dir pip setuptools numpy==1.15.3 scipy==1.1.0 scikit-learn==0.20.0 pandas==0.23.4 boto3==1.9.35 \
    && apk del --no-cache .build-deps

CMD ["python3"]

